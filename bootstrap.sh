#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# bootstrap.sh file for restore instance
# passed as startup-script parameter from restore.sh
set -eufo pipefail
IFS=$'\t\n'

error() {
  echo "ERROR - $(date +'%Y-%m-%dT%H:%M:%S%z')" "$@" | tee -a /var/tmp/bootstrap.log >&2
}

msg() {
  echo "INFO - $(date +'%Y-%m-%dT%H:%M:%S%z')" "$@" | tee -a /var/tmp/bootstrap.log
}

msg "Bootstrap start"

# Set global variables
export DEBIAN_FRONTEND=noninteractive
PSQL_BASE="/var/opt/gitlab/postgresql/data/"
PSQL_USER="gitlab-psql"
# those are set by amend.sh
CI_PROJECT_ID=will_be_set_on_CI
CI_VERIFY_JOB_ID=will_be_set_on_CI
INSTANCE_NAME=will_be_set_on_CI
RESTORE_TIME=$(date +"%F %H:%M:%S")

# Define functions
callback() {
  # interact with CI back
  # Arguments:
  # $1: what to do. Allowed now: play, cancel
  # $2: job ID
  local action="${1:-}"
  local job_id="${2:-}"

  if ! [[ "${action}" =~ ^(play|cancel)$ ]]; then
    error "Invalid action: '${action}', allowed: play, cancel"
    return 2
  fi

  if [[ -z "${job_id}" ]]; then
    error "Job id to ${action} is required!"
    return 3
  fi

  curl --request POST \
    --silent \
    --show-error \
    --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN:="later_from_secrets"}" \
    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/${job_id}/${action}"
}

function verify_callback {
  # Final step, trigger the verify job via API
  callback "play" "${CI_VERIFY_JOB_ID}"
}

trap verify_callback EXIT

if [[ -L "/dev/disk/by-id/google-${INSTANCE_NAME}-data-disk" ]]; then
  if [[ $(file -sL "/dev/disk/by-id/google-${INSTANCE_NAME}-data-disk") != *Linux* ]]; then
    mkfs.ext4 "/dev/disk/by-id/google-${INSTANCE_NAME}-data-disk"
  fi
  mkdir /var/opt/gitlab
  mount "/dev/disk/by-id/google-${INSTANCE_NAME}-data-disk" /var/opt/gitlab
fi

# Set apt config, update repos and disable postfix prompt
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash	# this also runs apt-get update
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'No configuration'"

# install everything in one go
apt-get -y install daemontools lzop gcc make python3 virtualenv python3-dev libssl-dev gitlab-ee ca-certificates postfix htop iotop
gitlab-ctl reconfigure && gitlab-ctl stop

# Install wal-e
mkdir -p /opt/wal-e /etc/wal-e.d/env
virtualenv --python=python3 /opt/wal-e
/opt/wal-e/bin/pip3 install --upgrade pip
/opt/wal-e/bin/pip3 install boto wal-e google-compute-engine google-cloud-storage gcloud

# This is required on GCP because I have no idea why, thank you Alex
rm -f /etc/boto.cfg

# Get secrets from KMS
mkdir -p /tmp/secrets
gsutil -m cp -r gs://gitlab-restore-secrets/postgres-01.db.prd.gitlab.com/* /tmp/secrets
for f in WALE_GS_PREFIX PRIVATE_TOKEN; do
  gcloud kms decrypt \
    --location=global \
    --keyring=gitlab-restore-ci-keyring \
    --key=gitlab-restore-ci-secrets-key \
    --plaintext-file=/etc/wal-e.d/env/${f} \
    --ciphertext-file=/tmp/secrets/${f}.enc
done

gcloud kms decrypt \
  --location=global \
  --keyring=gitlab-restore-ci-keyring \
  --key=gitlab-restore-ci-secrets-key \
  --plaintext-file=/etc/wal-e.d/gcs.json \
  --ciphertext-file=/tmp/secrets/gcs.json.enc

echo '/etc/wal-e.d/gcs.json' > /etc/wal-e.d/env/GOOGLE_APPLICATION_CREDENTIALS

# no need to cleanup /tmp/secrets, they are encrypted

# export and clean up private token for API callback that will start verify job
PRIVATE_TOKEN=$(cat /etc/wal-e.d/env/PRIVATE_TOKEN) && shred --remove /etc/wal-e.d/env/PRIVATE_TOKEN
export PRIVATE_TOKEN

# clean up data directory
rm -r ${PSQL_BASE} && mkdir -p ${PSQL_BASE} && chmod 0700 ${PSQL_BASE} && chown gitlab-psql:root ${PSQL_BASE}

# configure recovery from archive
cat > "${PSQL_BASE}/recovery.conf" <<RECOVERY
standby_mode = 'on'
restore_command = '/usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e wal-fetch -p 16 "%f" "%p"'
recovery_target_timeline = 'latest'
trigger_file = '/var/opt/gitlab/postgresql/trigger'
RECOVERY
chown "${PSQL_USER}":"${PSQL_USER}" "${PSQL_BASE}/recovery.conf"

msg "Restore latest backup cmd as postgres user and time it"
su - "${PSQL_USER}" -c "time /usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-fetch ${PSQL_BASE} LATEST"

# re-create postgresql.conf and others
gitlab-ctl reconfigure

# patch postgresql.conf settings
cat >> "${PSQL_BASE}/postgresql.auto.conf" <<-EOM
hot_standby = 'on'
wal_level = 'replica' # Otherwise, we cannot drop replication slots
max_replication_slots = 10
max_connections = 300
max_standby_archive_delay = -1
statement_timeout = 0
EOM

# start postgresql and tail the log until we're done
gitlab-ctl start postgresql

# wait for wal restore to complete
# Sample line we're catching here
# 2017-11-09 23:57:08 UTC LOG:  database system is ready to accept connections
_fin="LOG:\\s\\sdatabase\\ssystem\\sis\\sready\\sto\\saccept\\sread\\sonly\\sconnections"
while IFS= read -r line; do
  [[ ${line} =~ ${_fin} ]] && break
done < <(tail -n 100 -f /var/log/gitlab/postgresql/current)

# Remove existing replication slots - otherwise we'd keep WAL around forever
gitlab-psql -c 'select pg_drop_replication_slot(slot_name) from pg_replication_slots'

# Wait for recovery to reach RESTORE_TIME
while [[ "$(gitlab-psql -t -c "select pg_last_xact_replay_timestamp() > '${RESTORE_TIME}'")" =~ "f" ]]; do
  msg "Waiting for recovery to go beyond ${RESTORE_TIME}, currently at $(gitlab-psql -t -c "select pg_last_xact_replay_timestamp()" | head -n 1)"
  sleep 30
done

duration=${SECONDS}
msg "bootstrap.sh finished in $((duration / 60)) minutes $((duration % 60)) seconds."

size=$(du -hsc /var/opt/gitlab/postgresql/data | tail -1 | cut -f 1)
msg "data directory size is ${size}."
