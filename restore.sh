#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates the creation of postgres-gprd restore instance at GCE
# Ran either locally or on CI
set -eufo pipefail
IFS=$'\t\n'

source common.sh

# Create data disk for postgres data
gcloud compute disks create \
  "${GCE_DATADISK_NAME}" \
  --size "${GCE_DATADISK_SIZE}" \
  --type "${GCE_DATADISK_TYPE}"

# Create instance with bootstrap script
bash amend.sh    # but amend it first
if [[ "$PREEMPTIBLE" -eq "1" ]]; then
  use_preemptible="--preemptible"
else
  use_preemptible=""
fi;
gcloud compute instances create \
  "${INSTANCE_NAME}" \
  $use_preemptible \
  --boot-disk-size "30GB" \
  --boot-disk-type "pd-standard" \
  --disk "name=${GCE_DATADISK_NAME},device-name=${GCE_DATADISK_NAME}" \
  --machine-type "${GCE_INSTANCE_TYPE}" \
  --image-project "ubuntu-os-cloud" \
  --image-family "${RESTORE_IMAGE_FAMILY}" \
  --zone "${GCE_ZONE}" \
  --network "${GCE_NETWORK}" \
  --subnet "${GCE_SUBNET}" \
  --service-account "gitlab-restore-ci@gitlab-restore.iam.gserviceaccount.com" \
  --scopes "default,https://www.googleapis.com/auth/cloudkms" \
  --metadata-from-file startup-script=bootstrap.sh

# We don't need to keep disks when the instances is being deleted
gcloud compute instances set-disk-auto-delete \
  "${INSTANCE_NAME}" \
  --disk "${GCE_DATADISK_NAME}"

# We're done. The bootstrap.sh will trigger next job via API.
