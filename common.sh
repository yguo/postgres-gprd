#!/bin/bash
# vim: ai:ts=8:sw=8:noet
### Command requirements

error() {
  echo "$(date +'%Y-%m-%dT%H:%M:%S%z')" "$@" >&2
}

command -v jq >/dev/null 2>/dev/null || { error 'Please install jq utility'; exit 1; }
command -v gcloud >/dev/null 2>/dev/null || { error 'Please install gcloud utility'; exit 1; }

if [[ ! -z ${GITLAB_CI:-} ]]; then
  # get all job ids into variables
  # this *should* be futureproof against job addition
  # variable conversion example:
  # verify => CI_VERIFY_JOB_ID
  while IFS=' ' read -r k v; do
    export "$k"="$v"
  done <<< "$(curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
    --silent \
    --show-error \
    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs" \
    | jq '.[] | "CI_\(.name|ascii_upcase)_JOB_ID \(.id)"' | tr -d '"')"
else
  echo "Not on CI, skipping variable lookup!"
fi

# Name of the instance
if [[ -z "${INSTANCE_NAME:-}" ]]; then
  export INSTANCE_NAME="restore-postgres-gprd-${CI_PIPELINE_ID:-"default"}"
fi

# Location of the zone to create GCE instance in
export GCE_ZONE=${GCE_ZONE:-us-west1-a}
export GCE_NETWORK=${GCE_NETWORK:-default}
export GCE_SUBNET=${GCE_SUBNET:-default}

export GCE_DATADISK_NAME="${INSTANCE_NAME}-data-disk"
export GCE_INSTANCE_TYPE=${GCE_INSTANCE_TYPE:-n1-highcpu-16}
export GCE_DATADISK_TYPE=${GCE_DATADISK_TYPE:-pd-ssd}
export GCE_DATADISK_SIZE=${GCE_DATADISK_SIZE:-3000GB}

export SUCCESS_HOOK=${SUCCESS_HOOK:-""}
export RESTORE_IMAGE_FAMILY='ubuntu-1604-lts'

export NO_CLEANUP=${NO_CLEANUP:-0}
export PREEMPTIBLE=${PREEMPTIBLE:-0}

gcloud config set compute/zone "${GCE_ZONE}"
