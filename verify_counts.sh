#!/bin/bash
set -eufo pipefail
IFS=$'\t\n'

error() {
  echo "$(date +'%Y-%m-%dT%H:%M:%S%z')" "$@" >&2
}

# Count all tables once to force reading the database
for table in $(ON_ERROR_STOP=true gitlab-psql -t -c "select tablename from pg_tables where schemaname='public'"); do
  relation_size=$(ON_ERROR_STOP=true gitlab-psql -t -c "select count(*) from ${table}" | head -n 1 | grep -Eo "[0-9]+")
  echo "${table}: ${relation_size}"
done

# Check we have fresh data
tables=(projects issues events)
for table in "${tables[@]}"; do
  found=$(ON_ERROR_STOP=true gitlab-psql -t -c "select created_at > pg_last_xact_replay_timestamp() - INTERVAL '3 hours' from ${table} order by id desc limit 1" | head -n 1)
  if [[ $found =~ "f" ]]; then
    error "Did not find recent enough record in table ${table}, latest: $(ON_ERROR_STOP=true gitlab-psql -t -c "select created_at from ${table} order by id desc limit 1" | head -n 1)"
    exit 1
  else
    echo "Table ${table} contains at least one recent record."
  fi
done

# We may want to do more verification here: Expecting certain relation sizes, or even data
# We may also consider monitoring table counts and pushing them to prometheus
