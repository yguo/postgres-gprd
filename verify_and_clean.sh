#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates the verification of postgres-gprd restore instance at GCE
# Ran either locally or on CI
set -eufo pipefail
IFS=$'\t\n'

source common.sh

#######################################################################
# Perform cleanup: delete disks, instances, unless NO_CLEANUP=1
# Globals:
#   INSTANCE_NAME, GCE_DATADISK_NAME, NO_CLEANUP, BOOTSTRAP_DURATION
# Arguments:
#   None
# Returns:
#   None
#######################################################################
function cleanup_resources {
  if [[ "$NO_CLEANUP" -eq "1" ]]; then
    echo "Skip cleanup step"
    return
  fi

  echo "Cleaning up"

  # Cleanup instance
  gcloud compute instances delete \
    "${INSTANCE_NAME}" \
    --delete-disks=all \
    --quiet || true
}

trap cleanup_resources EXIT

echo "bootstrap.sh.log:"
gcloud compute ssh \
  "${INSTANCE_NAME}" \
  --command="sudo cat /var/tmp/bootstrap*.log"

ERRORS=0

gcloud compute scp verify_counts.sh "${INSTANCE_NAME}":~/verify_counts.sh || ERRORS+=1
gcloud compute ssh \
  "${INSTANCE_NAME}" \
  --command="sudo bash ~/verify_counts.sh" || ERRORS+=1

# TODO:
# Dump database logically, examine dump, maybe even archive

if [[ "${ERRORS}" -gt 0 ]]; then
  echo "ERRORS: ${ERRORS}"
else
  if [ "${SUCCESS_HOOK}" != "" ]; then
    echo 'Reporting success to monitoring...';
    curl "${SUCCESS_HOOK//CI_PIPELINE_ID/${CI_PIPELINE_ID}}"
  fi
fi
