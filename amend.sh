#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This file amends bootstrap.sh file with correct job id for callback
source common.sh

# If we're on CI, then amend bootstrap.sh with callback
if [[ ! -z ${GITLAB_CI:-} ]]; then
  # shellcheck disable=SC2181
  [[ $? -ne 0 ]] && exit $?
  # set proper values for functions in bootstrap.sh
  sed -i "s|^CI_PROJECT_ID=.*|CI_PROJECT_ID='${CI_PROJECT_ID}'|" bootstrap.sh
  sed -i "s|^CI_RESTORE_JOB_ID=.*|CI_RESTORE_JOB_ID='${CI_RESTORE_JOB_ID}'|" bootstrap.sh
  sed -i "s|^CI_VERIFY_JOB_ID=.*|CI_VERIFY_JOB_ID='${CI_VERIFY_JOB_ID}'|" bootstrap.sh
  sed -i "s|^CI_CLEANUP_JOB_ID=.*|CI_CLEANUP_JOB_ID='${CI_CLEANUP_JOB_ID}'|" bootstrap.sh
  sed -i "s|^INSTANCE_NAME=.*|INSTANCE_NAME='${INSTANCE_NAME}'|" bootstrap.sh
  sed -i "s|^SUCCESS_HOOK=.*|SUCCESS_HOOK='${SUCCESS_HOOK}'|" bootstrap.sh
else
  echo "Not on CI, skipping!"
fi
