[modeline]: # ( vim: set enc=utf-8 spell et ts=2 sw=2 )

### Description

This project implements a backup testing pipeline. It's purpose is to
detect whether or not the backup is actually restorable and in good
shape. After all, what good is a backup that cannot be restored?

This is implemented in a CI pipeline that we kick of daily on a
schedule. It can also be triggered manually at any time (parallel
pipelines are supported).

### High level flow

1. Start CI pipeline
1. Create a new GCE database instance (`restore.sh`)
1. New GCE instance boots with `bootstrap.sh`
1. Chef recipe gets applied (via the omnibus reconfigure) to provision the database box
1. Retrieve the latest base backup and configure postgres to catch up
   from the WAL archive
1. Perform point in-time recovery (PITR) to recover to the time when
   this pipeline started (roughly)
1. Perform verification of the backup (`verify_and_clean.sh`)
1. Notify monitoring of a successfull restore (using a web hook)
1. Cleanup of the GCE resources

### How to keep instance as archive replica

The database is configured to perform recovery from the WAL archive.
This means that as long as we keep the instance, it is going to stay up
to date with production.

In order to keep the instance alive, the pipeline needs to be cancelled
after the `restore` stage has finished. This keeps the verification job
from running and cleaning up afterwards.

### How to cleanup and how to keep resources

In case there are resources pending a cleanup (because we kept the
instance for archive recovery or because the pipeline failed for some
reason), the `verify` stage can be run manually. It is going to cleanup
all GCE resources in the end.

Alternatively, specify `NO_CLEANUP=1` as a CI variable in order to
prevent cleaning up GCE resource at the end of the process. This is
useful for example when wanting to examine or otherwise work with the
instance.

### GCE details

We can use preemptible instances to save costs by specifying
`PREEMPTIBLE=1` as a CI variable.

#### Starting in different regions or projects

It is possible to start the restore instance in any region. For that,
the CI pipeline takes these variables:

* `GCE_ZONE`, defaults to `us-west1-a`
* `GCE_NETWORK`, defaults to `default`
* `GCE_SUBNET`, defaults to `default`

In order to start the instance in a different GCP project, change
`GCE_PROJECT` accordingly (it defaults to `gitlab-restore`).

#### Setup of the GCE side
  - Create gitlab-restore project, link to billing
  - Create custom IAM role `CustomRole_GitLab_CI`, add the following permissions to it:
    - compute engine instance admin v1
    - service account user
    (There would be about 163 totale permissions added as per
    [iam docs](https://cloud.google.com/compute/docs/access/iam), so maybe this list can be
    reduced further)
  - Enable this role
  - Create service account `gitlab-restore-ci` with this custom role, save the json key.
  - `base64 custom.json` and save the output as a secret variable `GCE_KEY`
  - Edit service account's permissions and add it as `Service Account User` [see issue](https://gitlab.com/gitlab-com/infrastructure/issues/3609)

#### Secrets setup
At this point, the pipeline is able to create/destroy instances with specific bootstrap script. While there's no Vault yet,
lets handle secrets with KMS. The general idea is to have passwordless secrets symmetrically encrypted with Google's KMS system

Continue setup:
  - Enable KMS API under IAM -> Encryption Keys.
  - Create `gitlab-restore-ci-keyring` in global location.
  - Create `gitlab-restore-ci-secrets-key` key, with 30 days rotation period.
  - Add `gitlab-restore-ci` service account as role CryptoKey Decrypter and Encrypter (encrypter can be removed later)
  - Create a `gitlab-restore-secrets` nearline bucket to store encrypted data, and add `gitlab-restore-ci` service account as Object Viewer.
  - Export passwordless GPG key:
    - make sure gpg version is <2.1 or >2.1.13 as there's a bug that prevents exporting passwordless keys
    - import the existing key: `gpg --import ops-contact+dbcrypt.key`, provide passphrase
    - edit the imported key: `gpg --edit-key 89F75AAB4283C7A4262638DBA6F3999F66B9829C`
    - input `passwd` command in the gpg prompt; enter current passphrase, enter empty passphrase; confirm that no protection is needed
    - export armored key: `gpg --armor --batch --export-secret-keys 89F75AAB4283C7A4262638DBA6F3999F66B9829C > wal-e.gpg.key`. This should not ask for passphrase, and the key is compromised at this point. Just kidding.
    - **IMPORTANT** Delete the passwordless key from your gpg store: `gpg --delete-secret-keys 89F75AAB4283C7A4262638DBA6F3999F66B9829C`. Say `Y` twenty times or so.
  - Encrypt all the secrets: `mkdir -p postgres-01.db.prd.gitlab.com; for f in wal-e.gpg.key AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY WALE_S3_PREFIX WALE_GPG_KEY_ID AWS_REGION ; do gcloud kms encrypt --location=global --keyring=gitlab-restore-ci-keyring --key=gitlab-restore-ci-secrets-key --plaintext-file=${f} --ciphertext-file=./postgres-01.db.prd.gitlab.com/${f}.enc; done`
  - Upload `postgres-01.db.prd.gitlab.com` to `gitlab-restore-secrets` bucket
  - **IMPORTANT** shred and remove the secrets from local dir: `for f in wal-e.gpg.key AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY WALE_S3_PREFIX WALE_GPG_KEY_ID AWS_REGION ; do shred -u ${f}; done`
